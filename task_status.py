from argparse import ArgumentParser
from snakemake import logger as snakemake_logger
from enum import Enum
import requests
from uuid import UUID


class TaskStatus(Enum):
    """
    snakemake expects one of these options in stdout
    """

    SUCCESS = "success"
    FAILED = "failed"
    RUNNING = "running"


def query_task_status(task_id: UUID) -> TaskStatus:
    response = requests.get(url=f"http://localhost:8000/status/{task_id}")
    return TaskStatus(response.json()["status"])


arg_parser = ArgumentParser()
arg_parser.add_argument("task_id", type=str)
args = arg_parser.parse_args()

task_id = UUID(args.task_id.strip("'"))
task_status = query_task_status(task_id)
print(task_status.value)  # print to stdout for snakemake to pick up

if task_status == TaskStatus.FAILED:
    # TODO Query cluster for error message
    error_message = (
        f"############### JOB {args.task_id} failed with error. ###############"
    )
    snakemake_logger.error(error_message)
