from fastapi import FastAPI
from enum import Enum
from pprint import pprint
from uuid import uuid4, UUID
from pydantic import BaseModel
import logging

logger = logging.getLogger("uvicorn.access")
logger.setLevel(logging.INFO)

app = FastAPI()
TASK_ID = uuid4()


class NewTask(BaseModel):
    name: str
    args: str


@app.post("/submit")
async def submit_task(new_task: NewTask):
    pprint(new_task.json())
    return {"task_id": f"{TASK_ID}"}


class TaskStatus(Enum):
    """
    snakemake expects one of these options in stdout
    """

    SUCCESS = "success"
    FAILED = "failed"
    RUNNING = "running"


@app.get("/status/{task_id}")
async def task_status(task_id: UUID):
    return {"status": TaskStatus.RUNNING.value}
