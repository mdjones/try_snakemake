import os
from pathlib import Path

"""
Demonstrate a workflow with a step that produces a variable number of outputs which are not known before runtime
"""


rule all:
    input:
        "data/aggregated.txt",


checkpoint first_job:
    output:
        directory("data/first_job_outputs/"),
    params:
        job_name="FIRST_JOB",
    run:
        out_dir_path = Path("data/first_job_outputs")
        out_dir_path.mkdir(parents=True, exist_ok=True)
        for item_id in range(4):
            with open(f"data/first_job_outputs/{item_id}.txt", "w") as out:
                out.write(f"{item_id}")


# an intermediate rule
rule intermediate:
    input:
        "data/first_job_outputs/{i}.txt",
    output:
        "data/intermediate_job_outputs/{i}.txt",
    params:
        job_name="INTERMEDIATE_JOB",
    run:
        out_dir_path = Path(output[0]).parent
        out_dir_path.mkdir(parents=True, exist_ok=True)
        with open(input[0], "r") as infile:
            with open(output[0], "w") as out:
                out.write(infile.read())


def aggregate_input(wildcards):
    checkpoint_output = checkpoints.first_job.get(**wildcards).output[0]
    return expand(
        "data/intermediate_job_outputs/{i}.txt",
        i=glob_wildcards(os.path.join(checkpoint_output, "{i}.txt")).i,
    )


# an aggregation over all produced clusters
rule aggregate:
    input:
        aggregate_input,
    output:
        "data/aggregated.txt",
    params:
        job_name="AGGREGATE_JOB",
    run:
        with open(output[0], "w") as outfile:
            for file_name in input:
                with open(file_name, "r") as infile:
                    outfile.write(infile.read())
