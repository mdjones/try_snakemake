# Try snakemake

Sandbox for me to learn about [snakemake](https://github.com/snakemake/snakemake)

Snakemake docs: https://snakemake.readthedocs.io/en/stable/

Useful tools for use in developing with snakemake:
 - https://github.com/snakemake/snakefmt
 - PyCharm plugin (made by JetBrains!) https://plugins.jetbrains.com/plugin/11947-snakecharm
 - vscode extension https://marketplace.visualstudio.com/items?itemName=Snakemake.snakemake-lang

## How to run it

Create and activate the conda environment
```commandline
mamba env create
mamba activate try_snakemake
```

Run the fake cluster server with
```commandline
uvicorn cluster_server:app --reload --log-config log_config.yml
```
note that we specify `--log-config` to add timestamps to uvicorn logs.

From repository root directory run
```commandline
snakemake --profile .
```
this runs snakemake with a "profile": settings defined in `config.yaml`.

## Requirements of the cluster job runner

We need:
- a GET route to query task status by `id` and return whether successful, failed, or still running.
- the POST route to queue a new task needs to return the task id in its response

Would be very useful:
- a route to cancel a running task by task id. Transmit task cancel command in Worker heatbeat response?

## How snakemake knows when a cluster task is complete

We configure a command for snakemake to query the status of a task by specifying the following in the profile `config.yaml`:
```yaml
cluster-status: "python task_status.py"
```

Looking at the logs of our stub cluster server `cluster_server.py` we can see that snakemake polls for task status with a ~12 second period: 

```commandline
2023-06-24 20:14:58,006 - uvicorn.access - INFO - 127.0.0.1:52892 - "GET /status/1adba1d3-7898-4039-bdcc-8fd6597e8b28 HTTP/1.1" 200
2023-06-24 20:15:10,397 - uvicorn.access - INFO - 127.0.0.1:52897 - "GET /status/1adba1d3-7898-4039-bdcc-8fd6597e8b28 HTTP/1.1" 200
2023-06-24 20:15:22,809 - uvicorn.access - INFO - 127.0.0.1:52901 - "GET /status/1adba1d3-7898-4039-bdcc-8fd6597e8b28 HTTP/1.1" 200
2023-06-24 20:15:35,230 - uvicorn.access - INFO - 127.0.0.1:52904 - "GET /status/1adba1d3-7898-4039-bdcc-8fd6597e8b28 HTTP/1.1" 200
2023-06-24 20:15:47,638 - uvicorn.access - INFO - 127.0.0.1:52907 - "GET /status/1adba1d3-7898-4039-bdcc-8fd6597e8b28 HTTP/1.1" 200
```
