from uuid import UUID
from argparse import ArgumentParser

import requests
from snakemake.utils import read_job_properties
from typing import Dict


def queue_task(name: str, args: Dict) -> UUID:
    response = requests.post(
        url="http://localhost:8000/submit", json={"name": name, "args": "{}"}
    )
    return UUID(response.json()["task_id"])


arg_parser = ArgumentParser()
arg_parser.add_argument("task_script")
args = arg_parser.parse_args()

job_properties = read_job_properties(args.job_script)
job_args = job_properties["params"]
job_name = job_args.pop("job_name")

task_id = queue_task(name=job_name, args=job_args)
# print task id to first line of stdout from the script for snakemake to pick it up
print(f"{task_id}")
